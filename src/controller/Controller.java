package controller;

import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;
import com.opencsv.CSVReader;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VODaylyStatistic;
import model.vo.VOMovingViolations;
import model.vo.VOViolationCode;
import view.MovingViolationsManagerView;

public class Controller 
{

	private MovingViolationsManagerView view;
	public static Queue<VOMovingViolations> movingViolationsQueue;
	public static Stack<VOMovingViolations> movingViolationsStack;

	public Controller()
	{
		view = new MovingViolationsManagerView();
		movingViolationsQueue = new Queue<VOMovingViolations>();
		movingViolationsStack = new Stack<VOMovingViolations>();
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller controller = new Controller();

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				view.printMessage("Ingrese el cuatrimestre (1, 2 o 3)");
				int numeroCuatrimestre = sc.nextInt();
				controller.loadMovingViolations(numeroCuatrimestre);
				break;

			case 1:

				IQueue<VOMovingViolations> c = controller.verifyObjectIDIsUnique();
				boolean isUnique = c.isEmpty();
				view.printMessage("El objectId es único: " + isUnique);

				if(isUnique == false)
				{
					for(int i=0; i<c.size(); i++)
					{
						view.printMessage(c.dequeue().objectId()+"");
					}
				}
				break;

			case 2:

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2017-03-28T15:30:20.000Z)");
				LocalDateTime fechaInicialReq2A = convertirFecha_Hora_LDT(sc.next());

				view.printMessage("Ingrese la fecha con hora final (Ej : 2017-03-28T15:30:20.000Z)");
				LocalDateTime fechaFinalReq2A = convertirFecha_Hora_LDT(sc.next());

				IQueue<VOMovingViolations> resultados2 = controller.getMovingViolationsInRange(fechaInicialReq2A, fechaFinalReq2A);

				view.printMovingViolationsReq2(resultados2);

				break;

			case 3:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode3 = sc.next();

				double [] promedios3 = controller.avgFineAmountByViolationCode(violationCode3);

				view.printMessage("FINEAMT promedio sin accidente: " + promedios3[0] + ", con accidente:" + promedios3[1]);
				break;


			case 4:

				view.printMessage("Ingrese el ADDRESS_ID");
				String addressId4 = sc.next();

				view.printMessage("Ingrese la fecha con hora inicial (Ej : 2017-03-28)");
				LocalDate fechaInicialReq4A = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha con hora final (Ej : 2017-03-28)");
				LocalDate fechaFinalReq4A = convertirFecha(sc.next());

				IStack<VOMovingViolations> resultados4 = controller.getMovingViolationsAtAddressInRange(addressId4, fechaInicialReq4A, fechaFinalReq4A);

				view.printMovingViolationsReq4(resultados4);

				break;

			case 5:
				view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
				double limiteInf5 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
				double limiteSup5 = sc.nextDouble();

				IQueue<VOViolationCode> violationCodes = controller.violationCodesByFineAmt(limiteInf5, limiteSup5);
				view.printViolationCodesReq5(violationCodes);
				break;

			case 6:

				view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
				double limiteInf6 = sc.nextDouble();

				view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
				double limiteSup6 = sc.nextDouble();

				view.printMessage("Ordenar Ascendentmente: (Ej: true)");
				boolean ascendente6 = sc.nextBoolean();				

				IStack<VOMovingViolations> resultados6 = controller.getMovingViolationsByTotalPaid(limiteInf6, limiteSup6, ascendente6);
				view.printMovingViolationReq6(resultados6);
				break;

			case 7:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial7 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal7 = sc.nextInt();

				IQueue<VOMovingViolations> resultados7 = controller.getMovingViolationsByHour(horaInicial7, horaFinal7);
				view.printMovingViolationsReq7(resultados7);
				break;

			case 8:

				view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
				String violationCode8 = sc.next();

				double [] resultado8 = controller.avgAndStdDevFineAmtOfMovingViolation(violationCode8);

				view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviaciÃ³n estandar:" + resultado8[1]);
				break;

			case 9:

				view.printMessage("Ingrese la hora inicial (Ej: 23)");
				int horaInicial9 = sc.nextInt();

				view.printMessage("Ingrese la hora final (Ej: 23)");
				int horaFinal9 = sc.nextInt();

				int resultado9 = controller.countMovingViolationsInHourRange(horaInicial9, horaFinal9);

				view.printMessage("NÃºmero de infracciones: " + resultado9);
				break;

			case 10:
				double [] percent= porcentajeAccidentesPorHora();
				view.printMovingViolationsByHourReq10(percent);
				break;

			case 11:
				view.printMessage("Ingrese la fecha inicial (Ej : 2018-03-28)");
				LocalDate fechaInicial11 = convertirFecha(sc.next());

				view.printMessage("Ingrese la fecha final (Ej : 2018-03-28)");
				LocalDate fechaFinal11 = convertirFecha(sc.next());

				double resultados11 = controller.totalDebt(fechaInicial11, fechaFinal11);
				view.printMessage("Deuda total "+ resultados11);
				break;

			case 12:	
				int [] deudas= this.deudaPorMes();
				view.printTotalDebtbyMonthReq12(deudas);

				break;

			case 13:	
				fin=true;
				sc.close();
				break;
			}
		}

	}

	/**
	 * Carga las infracciones.
	 * @param numeroCuatrimestre Cuatrimestres escogido por el usuario.
	 */
	public void loadMovingViolations(int numeroCuatrimestre) 
	{
		if(numeroCuatrimestre == 1)	
			this.cargarInfracciones("./data/Enero_2018.csv", "./data/Febrero_2018.csv", "./data/Marzo_2018.csv", "./data/Abril_2018.csv");

		else if(numeroCuatrimestre == 2)
			this.cargarInfracciones("./data/Mayo_2018.csv", "./data/Junio_2018.csv", "./data/Julio_2018.csv", "./data/Agosto_2018.csv");

		else
			this.cargarInfracciones("./data/Septiembre_2018.csv", "./data/Octubre_2018.csv", "./data/Noviembre_2018.csv", "./data/diciembre_2018.csv");
	}
	/**
	 * Carga las infracciones del cuatrimestre con las rutas dadas por parámetro.
	 * @param ruta1 Primer mes del cuatrimestre.
	 * @param ruta2 Segundo mes del cuatrimestre.
	 * @param ruta3 Tercer mes del cuatrimestre.
	 * @param ruta4 Cuarto mes del cuatrimestre.
	 */
	public void cargarInfracciones(String ruta1, String ruta2, String ruta3, String ruta4)
	{

		CSVReader lector = null;
		String [] rutas = new String[4];
		rutas[0] = ruta1;
		rutas[1] = ruta2;
		rutas[2] = ruta3;
		rutas[3] = ruta4;

		for(int i = 0; i < rutas.length; i++)
		{
			try 
			{
				lector = new CSVReader(new FileReader(rutas[i]));
				String[] partes = lector.readNext();
				partes = lector.readNext();
				int contador = 0;

				while(partes != null)
				{
					int id = Integer.parseInt(partes[0]);
					String loc = partes[2];
					int add = Integer.parseInt(partes[3]);
					int str = Integer.parseInt(partes[4]);

					double fin = Double.parseDouble(partes[8]);
					double total = Double.parseDouble(partes[9]);
					double pen = Double.parseDouble(partes[10]);
					String acId = partes[12];
					String date;
					String vc;
					String desc;
					if(partes[13].contains("2018"))
					{
						date=partes[13];
						vc = partes[14];
						desc = partes[15];
					}
					else
					{
						date= partes[14];
						vc = partes[15];
						desc = partes[16];
					}

					VOMovingViolations nuevo = new VOMovingViolations(id, loc, add, str, fin, total, pen, acId, date, vc, desc);
					movingViolationsStack.push(nuevo);
					movingViolationsQueue.enqueue(nuevo);

					partes = lector.readNext();
					contador++;
				}
				lector.close();
				String[] m = rutas[i].split("/");
				String[] me = m[2].split("_");
				String mes = me[0];

				System.out.println("Elementos en el mes de "+mes+":"+contador);
			}
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		}

		System.out.println("Cantidad de elementos total :"+ movingViolationsQueue.size());
	}

	/**
	 * 
	 * @return Cola con la información.
	 */
	public IQueue <VODaylyStatistic> getDailyStatistics () 
	{
		int nInfracciones=0;
		int nAccidentes=0;
		double total = 0;
		IQueue<VODaylyStatistic> cola = null;

		if(movingViolationsQueue.size() == 0)
		{
			System.out.println("La cola esta vacia");
		}
		else
		{
			cola = new Queue<VODaylyStatistic>();
			Queue<VOMovingViolations> copia = (Queue<VOMovingViolations>) movingViolationsQueue;

			VOMovingViolations primero = copia.dequeue();
			while (primero != null)
			{
				String[] f = primero.getTicketIssueDate().split("T");
				String fActual = f[0];

				nInfracciones = 1;

				total += primero.getTotalPaid();
				if(primero.getAccidentIndicator().equals("yes"))
				{
					nAccidentes++;
				}
				VOMovingViolations siguiente = copia.dequeue();

				while(siguiente.getAccidentIndicator().startsWith(fActual))
				{
					nInfracciones++;
					total += siguiente.getTotalPaid();
					if(siguiente.getAccidentIndicator().equals("yes"))
						nAccidentes++;

					siguiente = copia.dequeue();
				}
				if(siguiente.getTicketIssueDate().startsWith(fActual)==false)
				{
					VODaylyStatistic nuevo = new VODaylyStatistic(fActual, nAccidentes, nInfracciones, total);
					cola.enqueue(nuevo);
					nInfracciones=0;
					nAccidentes=0;
					total=0;
					primero = copia.dequeue();
				}
			}
		}
		return cola;
	}

	/**
	 * 
	 * @param n
	 * @return
	 */
	public IStack <VOMovingViolations> nLastAccidents(int n) 
	{
		int nn = 0;
		IStack copia = movingViolationsStack;
		IStack<VOMovingViolations> pila = new Stack<VOMovingViolations>();

		VOMovingViolations objeto = (VOMovingViolations) copia.pop();
		while(nn<n)
		{
			if(objeto.getAccidentIndicator().equals("Yes"))
			{	
				int id = objeto.objectId();
				String date = objeto.getTicketIssueDate();
				String loc = objeto.getLocation();
				String descrip = objeto.getViolationDescription();
				pila.push(objeto);
				nn++;
				System.out.println(id+","+loc+","+date+","+descrip);
			}
			objeto = (VOMovingViolations) copia.pop();
		}
		return pila;
	}
	/**
	 * Verifica que el objectId de todas las infracciones sea único.
	 * @return Cola con las infracciones que tienen ObjectId repetido o cola vacía si no hay repetidos.
	 */
	public IQueue<VOMovingViolations> verifyObjectIDIsUnique()
	{
		Iterator<VOMovingViolations> it= movingViolationsQueue.iterator();
		IQueue<VOMovingViolations> idRepetidos= new Queue<VOMovingViolations>();
		boolean repetido=false;
		while(it.hasNext())
		{
			VOMovingViolations act= it.next();
			int id=act.objectId();
			Iterator<VOMovingViolations> iter= movingViolationsQueue.iterator();
			iter.next();
			while(iter.hasNext()&& !repetido)
			{
				VOMovingViolations sig= iter.next();
				if(id == sig.objectId())
				{
					repetido=true;
					idRepetidos.enqueue(act);
				}
			}
		}

		return idRepetidos;
	}

	/**
	 * Consulta las infracciones que ocurrieron en un rango de fecha dado.
	 * @param fechaInicial fecha inicial del rango a buscar.
	 * @param fechaFinal fecha final del rango a buscar.
	 * @return Una cola con las infracciones que ocurrieron en el rango de fecha dado.
	 */
	public IQueue<VOMovingViolations> getMovingViolationsInRange(LocalDateTime fechaInicial,LocalDateTime fechaFinal) 
	{
		Iterator<VOMovingViolations> it= movingViolationsStack.iterator();
		IQueue<VOMovingViolations> violationsInRange= new Queue<VOMovingViolations>();
		while(it.hasNext())
		{
			VOMovingViolations actual=it.next();
			LocalDateTime horaActual= convertirFecha_Hora_LDT(actual.getTicketIssueDate());
			if((horaActual.isAfter(fechaInicial) || horaActual.compareTo(fechaInicial)==0) && (horaActual.isBefore(fechaFinal) || horaActual.compareTo(fechaFinal)==0))
			{
				violationsInRange.enqueue(actual);
			}
		}
		return violationsInRange;
	}

	/**
	 * Consulta el promedio de la cantidad a pagar por el tipo de infraccion dado cuando hubo y cuando no hubo accidente.
	 * @param violationCode3 Tipo de infracción del que se quiere consultar el promedio a pagar.
	 * @return Un arreglo con el promedio a pagar del tipo de infracción cuando hubo y cuando no hubo infracción respectivamente.
	 */
	public double[] avgFineAmountByViolationCode(String violationCode3) 
	{
		double [] resp= new double [] {0.0 , 0.0};
		Iterator<VOMovingViolations> it= movingViolationsStack.iterator();
		int contAccidente=0;
		int contNoAccidente=0;
		double sumAccidente=0.0;
		double sumNoAccidente=0.0;
		while(it.hasNext())
		{
			VOMovingViolations actual=it.next();
			if(actual.getViolationCode().equals(violationCode3) && actual.getAccidentIndicator().equals("Yes"))
			{
				contAccidente++;
				sumAccidente+=actual.getFine();
				resp[0]=sumAccidente/contAccidente;
			}
			if(actual.getViolationCode().equals(violationCode3) && actual.getAccidentIndicator().equals("No"))
			{
				contNoAccidente++;
				sumNoAccidente+=actual.getFine();
				resp[1]=sumNoAccidente/contNoAccidente;
			}
		}
		return resp;
	}
	/**
	 * Consulta las infracciones que ocurrieron en una dirección y un rango de fecha dado.
	 * @param addressId Dirección en la que se quiere consultar las infracciones.
	 * @param fechaInicial fecha inicial del rango.
	 * @param fechaFinal fecha final del rango.
	 * @return Pila con las infracciones que ocurrieron en la dirección y rango de fecha dados.
	 */

	public IStack<VOMovingViolations> getMovingViolationsAtAddressInRange(String addressId,LocalDate fechaInicial, LocalDate fechaFinal)
	{
		Stack<VOMovingViolations> resultado= new Stack<VOMovingViolations>();
		Iterator<VOMovingViolations> it= movingViolationsStack.iterator();
		while(it.hasNext())
		{
			VOMovingViolations actual= it.next();
			String [] ticketIssueDate=actual.getTicketIssueDate().split("T");
			LocalDate fechaActual= convertirFecha(ticketIssueDate[0]);
			if(actual.getAddressId()== Integer.parseInt(addressId) && (fechaActual.isAfter(fechaInicial) || fechaActual.equals(fechaInicial)) && (fechaActual.isBefore(fechaFinal) || fechaActual.equals(fechaFinal)))
			{
				resultado.push(actual);
			}
		}
//		VOMovingViolations[] array = convertirPilaAArreglo(resultado);
//		ordenarPor(false,"street",array);
//		return convertirArrayAPila(array);
		return resultado;
	}

	/**
	 * Consulta los tipos de infracciones con su valor promedio en un rango dado.
	 * @param limiteInf5 Rango inferior a buscar.
	 * @param limiteSup5 Rango superior a buscar.
	 * @return Cola con los tipos de infracciones con su respectivo codigo y valor promedio.
	 */
	public IQueue<VOViolationCode> violationCodesByFineAmt(double limiteInf5, double limiteSup5) 
	{
		Iterator<VOMovingViolations> iter = movingViolationsQueue.iterator();
		Queue<VOViolationCode> c = new Queue<VOViolationCode>();
		
		String cc = "";
		while(iter.hasNext())
		{
			VOMovingViolations actual = iter.next();
			String code = actual.getViolationCode();
			double fine = actual.getFine();
			int contador = 0;
			double suma = 0;
			if(fine <= limiteSup5 && fine >= limiteInf5 && code!=cc)
			{
				contador++;
				suma += fine;

				Iterator<VOMovingViolations> iter2 = movingViolationsQueue.iterator();

				while(iter2.hasNext())
				{
					VOMovingViolations actual2 = iter2.next();
					String code2 = actual2.getViolationCode();
					double fine2 = actual2.getFine();

					if(code.equals(code2)&& actual.equals(actual2)==false && fine2 >= limiteInf5 && fine <= limiteSup5)
					{
						contador++;
						suma += actual.getFine();
					}
				}
			}
			double promedio = suma/contador;
			c.enqueue(new VOViolationCode(code, promedio));
			cc = code;
		}
		return c;
	}

	/**
	 * Consulta las infracciones donde la cantidad pagada esta dentro del rango dado. Las ordena por fecha de infracción descendente o asecendentemente.
	 * @param limiteInf6 Límite inferior de rango.
	 * @param limiteSup6 Límite superior del rango.
	 * @param ascendente6 Booleano, indica si se ordena ascendentemente (true) o desendentemente (false).
	 * @return Pila con las infracciones que cumplieron las condiciones anteriores.
	 */
	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(double limiteInf6, double limiteSup6,boolean ascendente6) 
	{
		IStack<VOMovingViolations> pila = new Stack<>();
		Iterator<VOMovingViolations> iter = movingViolationsStack.iterator();
		while(iter.hasNext())
		{
			VOMovingViolations actual = iter.next();
			double total = actual.getTotalPaid();
			if(total >= limiteInf6 && total <= limiteSup6)
			{
				pila.push(actual);
			}
		}

//		VOMovingViolations [] array = convertirPilaAArreglo(pila);
//		return convertirArrayAPila(ordenarPor(ascendente6, "fecha", array));
		return pila;
	}


	/**
	 * Consulta las infracciones por hora inicial y final ordenandolas ascendentemente por el codigo de la infracción.
	 * @param horaInicial7 Rango inicial de hora.
	 * @param horaFinal7 Rango final de hora.
	 * @return Cola con las infracciones que cumplieron las condiciones anteriores ordenadas ascendentemente por codigo de infracción.
	 */
	public IQueue<VOMovingViolations> getMovingViolationsByHour(int horaInicial7, int horaFinal7) 
	{
		Iterator<VOMovingViolations> iter = movingViolationsQueue.iterator();
		Queue<VOMovingViolations> c = new Queue<VOMovingViolations>();

		while(iter.hasNext())
		{
			VOMovingViolations actual = iter.next();
			String date = actual.getTicketIssueDate();
			String [] d = date.split("T");
			String []dd = d[1].split(":");
			int hora = Integer.parseInt(dd[0]);

			if(hora <= horaFinal7 && hora >= horaInicial7)
			{
				c.enqueue(actual);
			}
		}
//		VOMovingViolations[] array = convertirColaAArreglo(c);
//		return convertirArrayACola(ordenarPor(true, "descripcion", array));
		return c;
	}
	

	/**
	 * Informa la deuda promedio de un tipo de infracción y su desviación estándar.
	 * @param violationCode8 Código del tipo de infracción a buscar.
	 * @return Arreglo con el promedio y la desviacion estandar de la infracción.
	 */
	public double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode8)
	{
		double [] resp = new double [] {0.0 , 0.0};
		double promedio=0;

		Iterator<VOMovingViolations> it= movingViolationsStack.iterator();
		int contAccidente=0;
		double sumAccidente=0.0;

		while(it.hasNext())
		{
			VOMovingViolations actual=it.next();
			if(actual.getViolationCode().equals(violationCode8))
			{
				contAccidente++;
				sumAccidente+=actual.getFine();
				promedio = sumAccidente/contAccidente;
			}

		}

		Iterator<VOMovingViolations> iter = movingViolationsQueue.iterator();
		int contador = 0;
		double suma = 0.0;
		while(iter.hasNext())
		{
			VOMovingViolations actual=iter.next();
			if(actual.getViolationCode().equals(violationCode8))
			{
				contador++;
				suma+=Math.pow(actual.getFine()-promedio, 2);	
			}
		}
		double desviacion = Math.sqrt((suma/contador));
		resp[0]=promedio;
		resp[1] = desviacion;
		return resp;
	}

	/**
	 * Consulta la cantidad de infracciones que ocurrieron en un rango de hora dado teniendo 
	 * en cuenta las infracciones de todo el cuatrimestre.
	 * @param horaInicial9 hora inicial del rango.
	 * @param horaFinal9 hora final del rango.
	 * @return int- cantidad de infracciones que ocurrieron en el rango de hora dado.
	 */
	public int countMovingViolationsInHourRange(int horaInicial9, int horaFinal9) 
	{
		int contador=0;
		Iterator<VOMovingViolations> iter= movingViolationsStack.iterator();
		while(iter.hasNext())
		{
			String[] split=iter.next().getTicketIssueDate().split("T");
			String[] hora=split[1].split(":");
			if((Integer.parseInt(hora[0])>horaInicial9 || Integer.parseInt(hora[0])==horaInicial9) && (Integer.parseInt(hora[0])<horaFinal9 || Integer.parseInt(hora[0])==horaFinal9))
				contador++;
		}
		return contador;
	}

	/**
	 * Consulta la deuda total de las infracciones que ocurrieron en un rango de fecha.
	 * @param fechaInicial11 fecha inicial del rango.
	 * @param fechaFinal11 fecha final del rango.
	 * @return double- deuda total de las infracciones ocurridas en un rango de fecha.
	 */
	public double totalDebt(LocalDate fechaInicial11, LocalDate fechaFinal11)
	{
		System.out.println(movingViolationsQueue.size());
		double debt=0.0;
		LocalDateTime fechaInicial= fechaInicial11.atStartOfDay();
		LocalDateTime fechaFinal= fechaFinal11.atStartOfDay();
		IQueue<VOMovingViolations> violationsInRange= getMovingViolationsInRange(fechaInicial, fechaFinal);
		Iterator<VOMovingViolations> iter= violationsInRange.iterator();
		while(iter.hasNext())
		{
			VOMovingViolations actual= iter.next();
			debt+=actual.getFine()+actual.getPenalty1()-actual.getTotalPaid();
		}
		return debt;
	}
	/**
	 * Método auxiliar para graficar el porcentaje de las infracciones con accidente.
	 * @return Un arreglo con el porcentaje que corresponde a las infracciones de cada hora.
	 */

	public double[] porcentajeAccidentesPorHora()
	{
		double total= movingViolationsQueue.size();
		double [] porcentajes= new double[24];
		for(int i=0;i<porcentajes.length;i++)
		{
			Iterator<VOMovingViolations> iter= movingViolationsQueue.iterator();
			int cantAcc=0;
			while(iter.hasNext())
			{
				VOMovingViolations actual=iter.next();
				String[] split=actual.getTicketIssueDate().split("T");
				String[] hora=split[1].split(":");
				if(Integer.parseInt(hora[0])==i && actual.getAccidentIndicator().equals("Yes"))
				{
					cantAcc++;
				}
			}
			porcentajes[i]= (cantAcc*100)/total;
		}
		System.out.println("casiii"+porcentajes[23]);
		return porcentajes;
	}
	/**
	 * Método auxiliar para graficar la deuda por mes.
	 * @return Un arreglo con la deuda de cada mes.
	 */

	public int [] deudaPorMes()
	{
		int [] resp= new int[4];
		for (int i=1;i<resp.length+1;i++)
		{
			Iterator<VOMovingViolations> iter= movingViolationsQueue.iterator();
			while(iter.hasNext())
			{
				VOMovingViolations actual= iter.next();
				String mes=actual.getTicketIssueDate().split("T")[0].split("-")[1];
				if(Integer.parseInt(mes)==i || Integer.parseInt(mes)-4==i || Integer.parseInt(mes)-8==i )
				{
						resp[i-1]+=actual.getFine()+actual.getPenalty1()-actual.getTotalPaid();
				}
				
			}
		}
		resp[1]+=resp[0];
		resp[2]+=resp[1]+resp[0];
		resp[3]+=resp[2]+resp[1]+resp[0];
		return resp;
	}

	/**
	 * Ordena el arreglo por un criterio dado: Street, fecha, codigo o descripcion.
	 * @param pCriterio Criterio por el que se desea ordenar el arreglo.
	 * @param datos Arreglo de datos a ordenar.
	 * @return Arreglo ordenado.
	 */
	public VOMovingViolations[] metodoOrdenamiento(String pCriterio, VOMovingViolations[] datos)
	{
		int n = datos.length;
		int h = 1;
		while (h < n/3) 
			h = 3*h + 1; 

		while (h >= 1) 
		{
			for (int i = h; i < n; i++) 
			{
				for (int j = i; j >= h && less(datos[j], datos[j-h], pCriterio); j -= h) 
				{
					exchange(datos, j, j-h);
				}
			}

			h = h / 3;
		}
		return datos;
	}
	/**
	 * Ordena un arreglo de datos de menera ascendente o descendete según criterio.
	 * @param ascendente Boolean true si se desea ordenar ascendentemente, false si se quiere ordenar descendentemente.
	 * @param pCriterio Criterio por el que se desea ordenar el arreglo.
	 * @param datos Arreglo de datos a ordenar.
	 * @return Arreglo ordenado.
	 */
	public VOMovingViolations[] ordenarPor(boolean ascendente, String pCriterio, VOMovingViolations[] datos)
	{
		VOMovingViolations[] d = metodoOrdenamiento(pCriterio, datos);

		if(ascendente == false)
		{
			int end = d.length-1;
			for (int i=0;i<d.length && end>=d.length/2;i++)
			{
				VOMovingViolations copia= d[i];
				d[i]=d[end];
				d[end]=copia;
				end--;
			}

		}
		return d;
	}
	/**
	 * Compara 2 elementos por fecha.
	 * @param v1 Elemento 1.
	 * @param v2 Elemento 2.
	 * @return 1 si el elemento 1 es mayor al 2. -1 de lo contrario.
	 */
	public int compararPorFecha(VOMovingViolations v1, VOMovingViolations v2)
	{

		int retorno = 0;
		String date = v1.getTicketIssueDate();
		String date2 = v1.getTicketIssueDate();

		LocalDateTime d1 = convertirFecha_Hora_LDT(date);
		LocalDateTime d2 = convertirFecha_Hora_LDT(date2);


		if (d1.compareTo(d2) > 0 )
			retorno = 1;

		else if(d1.compareTo(d2) <0 )
			retorno = -1;

		return retorno;
	}
	/**
	 * Compara 2 elementos por StreetId. En caso de que sean iguales se compara por fecha.
	 * @param v1 Elemento 1.
	 * @param v2 Elemento 2.
	 * @return 1 si el elemento 1 es mayor al 2. -1 de lo contrario.
	 */
	public int compararPorStreetId(VOMovingViolations v1, VOMovingViolations v2)
	{
		int id1 = v1.getStreetId();
		int id2 = v2.getAddressId();

		if (id1 > id2)
			return 1;

		else if(id1 < id2)
			return-1;

		else
			return compararPorFecha(v1, v2);

	}
	/**
	 * Compara 2 elementos por codigo de la infracción.
	 * @param v1 Elemento 1.
	 * @param v2 Elemento 2.
	 * @return 1 si el elemento 1 es mayor al 2. -1 de lo contrario.
	 */
	public int compararPorViolationCode(VOMovingViolations v1, VOMovingViolations v2)
	{
		int retorno = 0;
		String c1 = v1.getViolationCode();
		String c2 = v2.getViolationCode();
		String[] n = c1.split("T");
		int cc1 = Integer.parseInt(n[1]);
		String[] n2 = c2.split("T");
		int cc2 = Integer.parseInt(n2[1]);

		if (cc1 > cc2)
			retorno = 1;

		else if(cc1 < cc2)
			retorno = -1;

		return retorno;
	}

	/**
	 * Compara 2 elementos por la descripción.
	 * @param v1 Elemento 1.
	 * @param v2 Elemento 2.
	 * @return 1 si el elemento 1 es mayor al 2. -1 de lo contrario.
	 */
	public int compararPorDescription(VOMovingViolations v1, VOMovingViolations v2)
	{
		int retorno = 0;
		String d1 = v1.getViolationDescription();
		String d2 = v2.getViolationDescription();

		if (d1.compareTo(d2) > 0)
			retorno = 1;

		else if(d1.compareTo(d2) < 0)
			retorno = -1;

		return retorno;
	}

	/**
	 * Convierte una cola en un arreglo.
	 * @param pCola Cola a convertir.
	 * @return Arreglo con los datos de la cola.
	 */
	public VOMovingViolations[] convertirColaAArreglo(IQueue<VOMovingViolations> pCola)
	{
		Queue<VOMovingViolations> copia = (Queue<VOMovingViolations>) pCola;
		VOMovingViolations[] arreglo = new VOMovingViolations[pCola.size()];
		for(int i=0; i<copia.size(); i++)
		{
			arreglo[i] = copia.dequeue();
		}
		return arreglo;
	}

	/**
	 * Convierte una pila en un arreglo.
	 * @param pPila Pila a convertir.
	 * @return Arreglo con los datos de la pila.
	 */
	public VOMovingViolations[] convertirPilaAArreglo(IStack<VOMovingViolations> pPila)
	{
		Stack<VOMovingViolations> copia = (Stack<VOMovingViolations>) pPila;
		VOMovingViolations[] arreglo = new VOMovingViolations[pPila.size()];
		for(int i=0; i<copia.size(); i++)
		{
			arreglo[i] = copia.pop();
		}
		return arreglo;
	}
	/**
	 * Intercambia dos elementos.
	 * @param datos Arreglo de datos
	 * @param i elemento i
	 * @param j elemento j
	 */
	public void exchange(VOMovingViolations[] datos, int i, int j)
	{
		VOMovingViolations d = datos[i];
		datos[i] = datos[j];
		datos[j] = d;
	}
	/**
	 * Compara dos elementos de acuero al criterio dado por parámetro.
	 * @param a Elemento 1
	 * @param b Elemento 2
	 * @param pCriterio Criterio por el que se va a comparar.
	 * @return True si el elemento 1 es menor al elemento 2 de acuerdo al criterio de comparación. False de lo contrario.
	 */
	public boolean less(VOMovingViolations a, VOMovingViolations b, String pCriterio)
	{
		if(pCriterio == "street")
			return compararPorStreetId(a, b)<0;
		else if(pCriterio == "codigo")
			return compararPorViolationCode(a, b)<0;
		else if(pCriterio == "fecha")
			return compararPorFecha(a, b)<0;
		else 
			return compararPorDescription(a, b)<0;	
	}
	/**
	 * Convierte un arreglo en una pila.
	 * @param datos Arreglo de elementos a convertir.
	 * @return Pila con los elementos del arreglo.
	 */
	public IStack<VOMovingViolations> convertirArrayAPila(VOMovingViolations[] datos)
	{
		IStack<VOMovingViolations> p = new Stack<>();
		for(int i=0; i<datos.length; i++)
		{
			p.push(datos[i]);
		}
		return p;
	}
	/**
	 * Convierte un arreglo en una Cola.
	 * @param datos Arreglo de elementos a convertir.
	 * @return Cola con los elementos del arreglo.
	 */
	public IQueue<VOMovingViolations> convertirArrayACola(VOMovingViolations[] datos)
	{
		IQueue<VOMovingViolations> c = new Queue<>();
		for(int i=0; i<datos.length; i++)
		{
			c.enqueue(datos[i]);
		}
		return c;
	}
	/**
	 * Convertir fecha a un objeto LocalDate
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @return objeto LD con fecha
	 */
	private static LocalDate convertirFecha(String fecha)
	{
		return LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaaTHH:mm:ss con dd para dia, mm para mes y aaaa para agno, HH para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}
}
