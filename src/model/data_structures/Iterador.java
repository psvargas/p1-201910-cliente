package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Iterador <T> implements Iterator<T>
{
	private Node<T> actual;


	public Iterador(Node<T> pPrimero)
	{
		actual = pPrimero;
	}

	public boolean hasNext() 
	{
		return actual != null;
	}

	public T next() 
	{
		if (actual==null)
		{
			throw new NoSuchElementException("No hay un siguiente.");
		}

		T e = actual.darElemento();
		actual = actual.darSiguiente(); 
		return e;
	}

}
