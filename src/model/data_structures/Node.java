package model.data_structures;

import java.io.Serializable;

public class Node<E> implements Serializable 
{
	private Node<E> anterior;
	private Node<E> siguiente;

	private E elemento;

	public Node(E pElemento)
	{
		elemento = pElemento;
		siguiente = null;
		anterior = null;
	}

	public E darElemento()
	{
return elemento;
	}
	
	public Node<E> darAnterior()
	{
		return anterior;
	}

	public Node<E> darSiguiente()
	{
		return siguiente;
	}
	
	public void cambiarAnterior(Node<E> pElemento)
	{
		anterior = pElemento;
	}
	
	public void cambiarSiguiente(Node<E> pElemento)
	{
		siguiente = pElemento;
	}
	 public void cambiarActual(E pElemento)
	 {
		 elemento = pElemento;
	 }
}
