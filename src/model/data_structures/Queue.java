package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Queue<T> implements IQueue<T>
{

	private Node<T> primero;
	private Node<T> ultimo;
	private int tamanio;

	public Queue()
	{
		primero = null;
		ultimo = null;
		tamanio = 0;
	}


	public Iterator<T> iterator() 
	{
		return new Iterador<T>(primero);
	}


	public boolean isEmpty() 
	{
		return primero == null;
	}


	public int size() 
	{
		return tamanio;
	}


	public void enqueue(T t) 
	{
		Node<T> nuevo = new Node<T>(t);
		
		if(tamanio == 0)
		{
			primero = nuevo;
			ultimo = nuevo;
			tamanio++;
		}
		else
		{
			Node<T> anterior = ultimo;
			anterior.cambiarSiguiente(nuevo);
			ultimo = nuevo;
			tamanio++;
		}	
	}


	public T dequeue() 
	{
		T eliminar = null;

		if (isEmpty()) 
		{
			throw new NoSuchElementException("La cola esta vacia.");
		}

		else
		{
			Node<T> antiguo = primero;
			eliminar = primero.darElemento();
			primero = antiguo.darSiguiente();
			antiguo.cambiarSiguiente(null);
			tamanio--;
		}
		return eliminar;
	}
}
