package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class Stack<T> implements IStack<T> 
{


	private Node<T> primero;     
	private int tamano;                

	public Stack()
	{
		primero = null;
		tamano = 0;
	}

	public Iterator<T> iterator()
	{
		return new Iterador<T>(primero);
	}


	public boolean isEmpty() 
	{
		return primero == null;
	}


	public int size() 
	{
		return tamano;
	}


	public void push(T t) 
	{
		Node<T> nuevo = new Node<T>(t);
		if(primero == null)
		{
			primero = nuevo;
			tamano++;
		}
		else
		{
			nuevo.cambiarSiguiente(primero);
			primero = nuevo;
			tamano++;
		}

	}


	public T pop() 
	{
		Node<T> eliminar = primero;
		if (primero == null)
		{
			System.out.println("La pila esta vacia.");
		}

		else if(primero != null && primero.darSiguiente()!=null )
		{
			eliminar.darSiguiente().cambiarAnterior(null);
			primero = primero.darSiguiente();
			tamano--;
		}
		else
		{
			primero = null;
			tamano--;
		}
		return eliminar.darElemento();
	}
}

