package model.vo;

public class VODaylyStatistic 

{
	private String fecha;

	private int nAccidentes;

	private int nInfracciones;

	private double total;

	public VODaylyStatistic(String pFecha, int pAccidentes, int pInfracciones, double pTotal)
	{
		fecha = pFecha;
		nAccidentes = pAccidentes;
		nInfracciones = pInfracciones;
		total = pTotal;
	}

	public String darFecha()
	{
		return fecha;
	}


	public int darAccidentes()
	{
		return nAccidentes;
	}


	public int darInfracciones()
	{
		return nInfracciones;
	}


	public double getFineATM()
	{
		return total;
	}
}
