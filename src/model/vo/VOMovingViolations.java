package model.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations
{
	private  int objectId;
	private String location;
	private int addressId;
	private int streetId;
	private double fine;
	private double totalPaid;
	private double penalty1;
	private String accidentIndicator;
	private String ticketIssueDate;
	private static String violationCode;
	private String description;


	public VOMovingViolations(int pObId, String pLoc, int pAddress, int pStreet, double pFine, double pTotalPaid, double pPenalty, String pAId, String pTdate, String pVCode, String pDesc )
	{
		objectId = pObId;
		location = pLoc;
		addressId = pAddress;
		streetId = pStreet;
		fine = pFine;
		totalPaid = pTotalPaid;
		penalty1 = pPenalty;
		accidentIndicator = pAId;
		ticketIssueDate = pTdate;
		violationCode = pVCode;
		description = pDesc;
	}
	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() 
	{
		return objectId;
	}	

	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() 
	{
		return location;
	}
	/**
	 * @return addressId - Id de la dirección.
	 */
	public int getAddressId()
	{
		return addressId;
	}
	/**
	 * @return streetId - Id del segmento de la calle.
	 */
	public int getStreetId()
	{
		return streetId;
	}
	/**
	 * @return fine - cantidad a pagar por la infracción.
	 */
	public double getFine()
	{
		return fine;
	}
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public double getTotalPaid() 
	{
		return totalPaid;
	}
	/**
	 * @return penalty1 - Dinero extra que debe pagar el conductor.
	 */
	public double getPenalty1()
	{
		return penalty1;
	}
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() 
	{
		return accidentIndicator;
	}
	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() 
	{
		return ticketIssueDate;
	}
	/**
	 * @return violationCode - código de la infracción.
	 */
	public static String getViolationCode()
	{
		return violationCode;
	}
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() 
	{
		return description;
	}
}

