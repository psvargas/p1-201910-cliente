package model.vo;

public class VOViolationCode 
{

	private String violationCode;
	private double promedio;
	
	public VOViolationCode(String code, double pPromedio)
	{
		violationCode = code;
		promedio = pPromedio;
		
	}
	public String getViolationCode() 
	{
		return violationCode;
	}
	
	public double getAvgFineAmt() 
	{
		return promedio;
	}
	
}

