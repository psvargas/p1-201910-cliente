package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class TestQueue <T> extends TestCase{

	private Queue<Node> cola;
	private Node<String> o1= new Node("o1");
	private Node<String> o2= new Node("o2");
	private Node<String> o3= new Node("o3");
	private Node<String> o4= new Node("o4");
	private Node<String> o5= new Node("o5");
	private Node<String> o6= new Node("o6");
	private Node<String> o7= new Node("o7");
	private Node<String> o8= new Node("o8");
	private Node<String> o9= new Node("o9");
	private Node<String> o10= new Node("o10");
	
	@Before
	//Prueba con la cola con 10 elementos.
	public void setUpEscenario0()
	{
		cola= new Queue();
		cola.enqueue(o1);
		cola.enqueue(o2);
		cola.enqueue(o3);
		cola.enqueue(o4);
		cola.enqueue(o5);
		cola.enqueue(o6);
		cola.enqueue(o7);
		cola.enqueue(o8);
		cola.enqueue(o9);
		cola.enqueue(o10);
	}
	//Prueba con la cola vac�a.
	public void setUpEscenario1()
	{
		cola = new Queue();
	}
	//Prueba con la cola con 50 elementos.
	public void setUpEscenario2()
	{
		cola= new Queue();
		for (int i=1;i<51;i++)
		{
			Node <String> nuevo= new Node<String>("o"+i);
			cola.enqueue(nuevo);
		}
	}
	@Test
	public void testDequeue() 
	{
		setUpEscenario1();
		try
		{
			cola.dequeue();
			fail("La cola esta vac�a.");
		}
		catch(Exception e)
		{
			//Debe lanzar Excepci�n.
		}
		
		setUpEscenario0();
		cola.dequeue();
		assertEquals("Error al eliminar objeto de la cola.", o1.darElemento(), cola.dequeue());
		
		setUpEscenario2();
		cola.dequeue();
		assertEquals("Error al eliminar objeto de la cola.", "o1", cola.dequeue());
	}
	
	@Test
	public void testEnqueue()
	{
		setUpEscenario1();
		Node <String> o1= new Node ("o1");
		cola.enqueue(o1);
		assertEquals("Error al agregar objeto a la cola vac�a.", 1, cola.size());
		
		setUpEscenario0();
		Node <String> o11= new Node ("o11");
		cola.enqueue(o11);
		assertEquals ("Fallo al agregar objeto a la cola.", 11, cola.size());
		
		setUpEscenario2();
		Node <String> nuevo2= new Node ("o51");
		cola.enqueue(nuevo2);
		assertEquals("Fallo al agregar objeto a la cola.",51, cola.size());
	}
	
	@Test
	public void testIsEmpty()
	{
		setUpEscenario1();
		assertEquals("Al inicio la cola deber�a estar vac�a.", true, cola.isEmpty());
		
		setUpEscenario0();
		assertEquals("La lista no deber�a estar vac�a.", false, cola.isEmpty());
		
		setUpEscenario2();
		assertEquals("La lista no deber�a estar vac�a.", false, cola.isEmpty());
	}
	
	@Test
	public void testIterator()
	{
		setUpEscenario1();
		assertNotNull("El iterador de la cola vac�a debe ser vac�o.", cola.iterator());
		assertFalse("El iterador de a cola vac�a no debe tener siguiente.",cola.iterator().hasNext());
		try
		{
			cola.iterator().next();
			fail("Sin elementos en la cola no debe avanzar a un siguiente.");
			
		}
		catch(Exception e)
		{
			//Deber�a lanzar excepci�n.
		}
		setUpEscenario0();
		assertTrue("Deber�a avanzar al siguiente elemento.", cola.iterator().hasNext());
		
		setUpEscenario2();
		assertTrue("Deber�a avanzar al siguiente elemento.", cola.iterator().hasNext());
	}
	
	@Test
	public void testSize()
	{
		setUpEscenario1();
		assertEquals("La cola esta vac�a", 0, cola.size());
		
		setUpEscenario0();
		assertEquals("Error al retornar el tama�o de la cola.", 10, cola.size());
		cola.enqueue(new Node<String> ("o11"));
		assertEquals("Al agregar un elemento el tama�o debe aumentar.", 11, cola.size());
		
		setUpEscenario2();
		assertEquals("Error al retornar el tama�o de la cola.", 50, cola.size());
		cola.dequeue();
		assertEquals("Al eliminar un elemento el tama�o debe disminuir.", 49, cola.size());
	}
	
}
