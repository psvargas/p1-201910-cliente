package model.data_structures;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class TestStack extends TestCase{

	private Stack<Node> pila;
	private Node<String> o1= new Node("o1");
	private Node<String> o2= new Node("o2");
	private Node<String> o3= new Node("o3");
	private Node<String> o4= new Node("o4");
	private Node<String> o5= new Node("o5");
	private Node<String> o6= new Node("o6");
	private Node<String> o7= new Node("o7");
	private Node<String> o8= new Node("o8");
	private Node<String> o9= new Node("o9");
	private Node<String> o10= new Node("o10");

	@Before
	//Prueba con 10 elementos.
	public void setUpEscenario0() 
	{
		pila= new Stack();
		pila.push(o1);
		pila.push(o2);
		pila.push(o3);
		pila.push(o4);
		pila.push(o5);
		pila.push(o6);
		pila.push(o7);
		pila.push(o8);
		pila.push(o9);
		pila.push(o10);
	}
	//Prueba con la pila vac�a.
	public void setUpEscenario1()
	{
		pila= new Stack();
	}
	//Prueba con 50 elementos.
	public void setUpEscenario2()
	{
		pila= new Stack();
		for (int i=1;i<51;i++)
		{
			Node <String> nuevo=new Node <String>("o"+i);
			pila.push(nuevo);
		}
	}
	@Test
	public void testIsEmpty() 
	{
		setUpEscenario1();
		assertEquals("Al inicio la pila deber�a estar vac�a.", true, pila.isEmpty());

		setUpEscenario0();
		assertEquals("La lista no deber�a estar vac�a.", false, pila.isEmpty());
		
		setUpEscenario2();
		assertEquals("La lista no deber�a estar vac�a.", false, pila.isEmpty());
	}

	@Test
	public void testIterator()
	{
		setUpEscenario1();
		assertNotNull("El iterador de la pila vac�a debe ser vac�o.", pila.iterator());
		assertFalse("El iterador de a pila vac�a no debe tener siguiente.",pila.iterator().hasNext());
		try
		{
			pila.iterator().next();
			fail("Sin elementos en la pila no debe avanzar a un siguiente.");

		}
		catch(Exception e)
		{
			//Deber�a lanzar excepci�n.
		}
		setUpEscenario0();
		assertTrue("Deber�a avanzar al siguiente elemento.", pila.iterator().hasNext());
		
		setUpEscenario2();
		assertTrue("Deber�a avanzar al siguiente elemento.", pila.iterator().hasNext());
	}
	@Test 
	public void testPop()
	{
		setUpEscenario1();
		try
		{
			pila.pop();
			fail("La pila esta vac��a.");
		}
		catch(Exception e)
		{
			//Debe lanzar excepci�n.
		}

		setUpEscenario0();
		pila.pop();
		assertEquals("Error al eliminar objeto de la pila", o10.darElemento(), pila.pop());
		
		setUpEscenario2();
		pila.pop();
		assertEquals("Error al eliminar objeto de la pila", "o50", pila.pop());
	}
	@Test
	public void testPush()
	{
		setUpEscenario1();
		pila.push(o1);
		assertEquals("Error al agregar objeto a la pila.",1, pila.size());
		
		setUpEscenario0();
		pila.push(new Node<String> ("o11"));
		assertEquals("Error al agregar objeto a la pila.", 11, pila.size());
		
		setUpEscenario2();
		pila.push(new Node<String> ("o51"));
		assertEquals("Error al agregar objeto a la pila.", 51, pila.size());
	}

	@Test
	public void testSize()
	{
		setUpEscenario1();
		assertEquals("La pila esta vac�a", 0, pila.size());

		setUpEscenario0();
		assertEquals("Error al retornar el tama�o de la pila.", 10, pila.size());
		pila.push(new Node<String> ("o11"));
		assertEquals("Al agregar un elemento el tama�o debe aumentar.", 11, pila.size());
		
		setUpEscenario2();
		assertEquals("Error al retornar el tama�o de la pila.", 50, pila.size());
		pila.pop();
		assertEquals("Al eliminar un elemento el tama�o debe disminuir.", 49, pila.size());
	}
}
